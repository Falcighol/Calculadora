package com.classes;

/**
 * @className Operations 
 * @date 18/07/2018
 * @version 1.0
 * @author Juan Pablo Elias Hernández
 */
public interface Operations {
    public void sumar(double a, double b);
    public void restar(double a, double b);
    public void multiplicar(double a, double b);
    public void dividir(double a, double b);
    public void potencia(double a, double b);
    public void raiz(double a);
}
