package com.classes;

/**
 * @className Calculator 
 * @date 18/07/2018
 * @version 1.0
 * @author Juan Pablo Elias Hernández
 */
public class Calculator implements Operations 
{
    /**
     * Variable para total de operaciones
     */
    private double total;
    
    @Override
    public void sumar(double a, double b)
    {
        this.total = a + b;
    }
    
    @Override
    public void restar(double a, double b)
    {
        this.total = a - b;
    }

    @Override
    public void multiplicar(double a, double b)
    {
        this.total = a * b;
    }

    @Override
    public void dividir(double a, double b)
    {
        this.total = a / b;
    }

    @Override
    public void potencia(double a, double b)
    {
        this.total = Math.pow(a, b);
    }

    @Override
    public void raiz(double a)
    {
        this.total = Math.sqrt(a);
    }

    public double getTotal()
    {
        return total;
    }

    public void setTotal(double total)
    {
        this.total = total;
    }

}
